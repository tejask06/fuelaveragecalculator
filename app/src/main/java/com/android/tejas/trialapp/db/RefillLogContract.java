package com.android.tejas.trialapp.db;

import android.provider.BaseColumns;

/**
 * Created by Tejas on 14-May-2015.
 */
public final class RefillLogContract {
    public RefillLogContract() {
    }

    public static abstract class RefillLog implements BaseColumns {
        public static final String TABLE_NAME = "refill_log";
        public static final String COLUMN_NAME_ODOMETER_READING = "odometer_reading";
        public static final String COLUMN_NAME_FUEL_READING = "fuel_reading";
        public static final String COLUMN_NAME_CREATION_DATE = "creation_date";
    }
}
