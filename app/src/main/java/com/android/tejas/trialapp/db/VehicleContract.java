package com.android.tejas.trialapp.db;

import android.provider.BaseColumns;

/**
 * Created by Tejas on 18-May-2015.
 */
public final class VehicleContract {
    public VehicleContract() {
    }

    public static abstract class Vehicle implements BaseColumns {
        public static final String TABLE_NAME = "vehicles";
        public static final String COLUMN_NAME_VEHICLE_NAME = "vehicle_name";
        public static final String COLUMN_NAME_CREATION_DATE = "creation_date";
    }
}
