package com.android.tejas.trialapp;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.android.tejas.trialapp.db.RefillLogDbHelper;
import com.android.tejas.trialapp.listeners.DrawerListItemClickListener;

import static com.android.tejas.trialapp.db.RefillLogContract.RefillLog.COLUMN_NAME_CREATION_DATE;
import static com.android.tejas.trialapp.db.RefillLogContract.RefillLog.COLUMN_NAME_FUEL_READING;
import static com.android.tejas.trialapp.db.RefillLogContract.RefillLog.COLUMN_NAME_ODOMETER_READING;
import static com.android.tejas.trialapp.db.RefillLogContract.RefillLog.TABLE_NAME;
import static com.android.tejas.trialapp.db.RefillLogContract.RefillLog._ID;


public class MainActivity extends ActionBarActivity {
    public static final String EXTRA_MESSAGE = "com.android.tejas.trialapp.MESSAGE";
    public static RefillLogDbHelper rDbHelper;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private String[] drawerListItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rDbHelper = new RefillLogDbHelper(this);
        drawerListItems = getResources().getStringArray(R.array.drawer_list_items);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, drawerListItems));
        mDrawerList.setOnItemClickListener(new DrawerListItemClickListener());
/*
        populateListView();
        Button submitButton = (Button) findViewById(R.id.button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText odometer = (EditText) findViewById(R.id.editText);
                EditText fuel = (EditText) findViewById(R.id.editText2);
                Context context = MainActivity.this;
                String fuelReading = fuel.getText().toString();
                String odometerReading = odometer.getText().toString();
                if (StringUtils.isEmpty(odometerReading)) {
                    odometer.setError("Odometer reading required!");
                    return;
                }
                SQLiteDatabase db = rDbHelper.getWritableDatabase();
                String[] projection = {_ID, COLUMN_NAME_ODOMETER_READING, COLUMN_NAME_FUEL_READING};
                String limit = "1";
                String sortOrder = COLUMN_NAME_CREATION_DATE + " DESC";
                Cursor c = db.query(TABLE_NAME, projection, null, null, null, null, sortOrder, limit);
                if (c.getCount() > 0) {
                    if (StringUtils.isEmpty(fuelReading)) {
                        fuel.setError("Fuel reading required!");
                        return;
                    }
                    c.moveToFirst();
                    Integer lastReading = c.getInt(c.getColumnIndex(COLUMN_NAME_ODOMETER_READING));
                    Integer distance = Integer.parseInt(odometerReading) - lastReading;
                    ((TextView) findViewById(R.id.textView)).setText(String.valueOf(distance / Float.parseFloat(fuelReading)));
                }
                ContentValues values = new ContentValues();
                values.put(COLUMN_NAME_ODOMETER_READING, odometerReading);
                values.put(COLUMN_NAME_FUEL_READING, fuelReading);
                db.insert(TABLE_NAME, COLUMN_NAME_FUEL_READING, values);
                populateListView();
            }
        });
*/
    }

    private void populateListView() {
        SQLiteDatabase db = rDbHelper.getWritableDatabase();
        String[] projection = {_ID, COLUMN_NAME_ODOMETER_READING, COLUMN_NAME_FUEL_READING};
        String sortOrder = COLUMN_NAME_CREATION_DATE + " DESC";
        Cursor c = db.query(TABLE_NAME, projection, null, null, null, null, sortOrder);
        ListView list = (ListView) findViewById(R.id.listView);
        String[] columns = new String[]{COLUMN_NAME_ODOMETER_READING, COLUMN_NAME_FUEL_READING};
        int[] to = new int[]{R.id.odometer, R.id.fuel,};
        SimpleCursorAdapter dataAdapter = new SimpleCursorAdapter(this, R.layout.list_view_row, c, columns, to, 0);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(dataAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendMessage(View view) {
/*
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText et = (EditText) findViewById(R.id.edit_message);
        String message = et.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
*/
    }
}
